from dolfin import *

mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)
R = FunctionSpace(mesh, "R", 0)

#u = interpolate(Expression("x[0]*(1-x[0])*x[1]*(1-x[1])"), V)
u = Function(V)
v = TestFunction(V)

f = Constant(1.0); g = Constant(0.0)
epsilon = Constant(1.0e-5); p = interpolate(Constant(2), R)
bc = DirichletBC(V, g, DomainBoundary())

def gamma(u):
  return (epsilon**2 + 0.5 * inner(grad(u), grad(u)))**((p-2)/2)
F = inner(grad(v), gamma(u) * grad(u))*dx - inner(f, v)*dx

solve(F == 0, u, bc, solver_parameters={"newton_solver": {"maximum_iterations": 100}})

dp = interpolate(Constant(3), R)
du = Function(V)
dFdp = derivative(F, p, dp)
dFdu = derivative(F, u, du)
G = dFdu + dFdp
solve(G == 0, du, bc)

u.assign(u + du)
p.assign(Constant(5))
solve(F == 0, u, bc, solver_parameters={"newton_solver": {"maximum_iterations": 100}})
