from dolfin import *

mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

u = interpolate(Expression("x[0]*(1-x[0])*x[1]*(1-x[1])"), V)
v = TestFunction(V)

f = Constant(1.0); g = Constant(0.0)
epsilon = Constant(1.0e-5); p = Constant(3)
bc = DirichletBC(V, g, DomainBoundary())

def gamma(u):
  return (epsilon**2 + 0.5 * inner(grad(u), grad(u)))**((p-2)/2)
F = inner(grad(v), gamma(u) * grad(u))*dx - inner(f, v)*dx

solve(F == 0, u, bc)
plot(u, interactive=True)
