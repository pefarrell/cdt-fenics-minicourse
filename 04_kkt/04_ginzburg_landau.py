from dolfin import *

# Define discrete Functionspace
mesh = RectangleMesh(0, 0, 1, 2, 100, 100)
U = FunctionSpace(mesh, "CG", 1) # Space for forward solution
V = FunctionSpace(mesh, "CG", 1) # Space for adjoint solution
M = FunctionSpace(mesh, "CG", 1) # Space for control
Z = MixedFunctionSpace([U, V, M])

# Define Functions
z = Function(Z)
(u, lmbda, m) = split(z)

# Define variational problem
F = inner(grad(u), grad(lmbda))*dx - m*lmbda*ds + inner(u**3, lmbda)*dx - inner(u, lmbda)*dx

# Define functional
ud = Constant(3)
J = 0.5*inner(u-ud, u-ud)*dx + 0.5*Constant(1e-7)*inner(m, m)*ds + 0.5*Constant(1.0e-10)*inner(m, m)*dx

# Derive optimality conditions
L = J + F
kkt = derivative(L, z, TestFunction(Z))

# Solve Poisson problem
solve(kkt == 0, z)

m = z.split()[2]
File("results/control.pvd") << m

u = z.split()[0]
File("results/solution.pvd") << u
