from dolfin import *

# Define discrete Functionspace
mesh = UnitSquareMesh(100, 100)
V = FunctionSpace(mesh, "CG", 1) # Space for forward solution
M = FunctionSpace(mesh, "DG", 0) # Space for control
Z = MixedFunctionSpace([V, V, M])

# Define Functions
z = Function(Z)
(u, lmbda, m) = split(z)

# Define variational problem
F = inner(grad(u), grad(lmbda))*dx - m*lmbda*dx

# Define functional
ud = Expression("sin(pi*x[0])*sin(pi*x[1])")   # Desired temperature profile
J = inner(u-ud, u-ud)*dx + Constant(1e-6)*inner(m, m)*dx

# Define boundary conditions
bc_u    = DirichletBC(Z.sub(0), 0.0, "on_boundary")
bc_lmbd = DirichletBC(Z.sub(1), 0.0, "on_boundary")
bcs = [bc_u, bc_lmbd]

# Derive optimality conditions
L = J + F
kkt = derivative(L, z, TestFunction(Z))

# Solve Poisson problem
solve(kkt == 0, z, bcs)
