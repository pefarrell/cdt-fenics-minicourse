from dolfin import *

mesh = UnitSquareMesh(32, 32)
V = FunctionSpace(mesh, "Lagrange", 1)

u = Function(V)
v = TestFunction(V)

f = Constant(-6.0)
g = Expression("1 + x[0]*x[0] + 2*x[1]*x[1]")
bc = DirichletBC(V, g, DomainBoundary())

F = inner(grad(u), grad(v))*dx - f*v*dx

solve(F == 0, u, bc)
plot(u, interactive=True)
